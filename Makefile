build:
	sudo docker build -t ds_solr .

inspect:
	sudo docker run --rm=true -ti ds_solr /bin/bash

daemon:
	-sudo docker stop ds_solr_sample_c1
	-sudo docker rm ds_solr_sample_c1
	sudo docker run -d --name=ds_solr_sample_c1 -p 8983:8983 ds_solr 
